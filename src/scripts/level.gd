extends Node3D
@onready var music = $music

var music_files = [
	"res://assets/music/Final_Lap.ogg",
	"res://assets/music/Source_Code_Synthwave.ogg",
	"res://assets/music/Transistor_Stomp.ogg",
	"res://assets/music/Underground.ogg"
]

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	music_looper()
	if Input.is_action_just_pressed("next_song"):
		Global.message = "Loading New Song"
		load_music()
func music_looper():
	if !music.is_playing():
		load_music()
		
func load_music():
	randomize()
	music_files.shuffle()
	var song = music_files[0]
	song = load(song)
	music.stream = song
	music.play()
