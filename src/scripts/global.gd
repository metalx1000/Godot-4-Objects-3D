extends Node

var music_volume = 0
var max_music_volume = 15
var min_music_volume = -20

var message = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	music_change_volume()
	
func music_change_volume():	
	if Input.is_action_just_pressed("music_volume_up"):
		music_volume += 1
		if music_volume > max_music_volume:
			music_volume = max_music_volume
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("music"), music_volume)
		message = "Music Volume: " + str(music_volume)
	if Input.is_action_just_pressed("music_volume_down"):
		music_volume -= 1
		if music_volume < min_music_volume:
			music_volume = min_music_volume
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("music"), music_volume)
		message = "Music Volume: " + str(music_volume)
		
	
