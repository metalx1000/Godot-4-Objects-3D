extends Control

@onready var player = get_tree().get_nodes_in_group("players")[0]
@onready var stats = $stats
@onready var msg = $msg
@onready var msg_timer = $msg_timer

var music_volume = Global.music_volume

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	stats_update()
	msg_update()
	pass

func msg_update():
	if Global.message != "":
		msg_timer.start()
		msg.text = Global.message
		Global.message = ""
		


func stats_update():
	stats.text = "Health: " + str(player.health)
	stats.text += "\nCoins: " + str(player.coins)



func _on_msg_timer_timeout():
	msg.text = ""
