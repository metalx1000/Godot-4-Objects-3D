extends Node3D

@onready var animation = $AnimationPlayer
@export var value = 1
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotate_y(.04)



func _on_area_3d_body_entered(body):
	if !body.is_in_group("players"):
		return
		
	body.coins += value
	animation.play("pickup")
	

