extends Node3D

@export var amount = 25
@onready var animation = $AnimationPlayer
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotate_y(.02)



func _on_area_3d_body_entered(body):
	if !body.is_in_group("players"):
		return
	
	if body.health >= 100:
		return
		
	animation.play("pickup")
	body.health += amount
	if body.health > 100:
		body.health = 100
	
